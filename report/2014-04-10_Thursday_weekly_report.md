# Thursday, 10 Apr 2014 23:46:24 +0700 - Weekly report #

## Tổng kết ##

* [bootstrap] Hoàn thành prototype trang home, categories của dự án Boardgame
* [wordpress] Hoàn thành một số back-end về hiện thị categories của dự án Boardgame
* [bitbucket] Sử dụng cơ bản bitbucket như remote add, clone, fetch, merge, rebase, pull request
* [slack] Sử dụng thành thạo slack
* [alogirm] Học được một số thuật toán từ bài test
* [theory] Hiểu rõ hơn về lý thuyết CMS, font-end, back-end, công cụ IDE và editor
* [convention] Học được cách sử dụng md, cách viết convention đúng quy tắc

## Kế hoạch tuần tới ##

* [wordpress] *Hoàn thành* dự án Boardgame 
* [alogirm] *Tìm hiểu* về clojure và *học* về các thuật toán



