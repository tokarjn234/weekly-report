# Friday, 17 Apr 2014 24:00:00 +0700 - Weekly report #

## Tổng kết ##

* [bootstrap, wordpress] Chỉnh sửa, hoàn thiện dự án Boardgame dựa trên những yêu cầu mới của khách hàng
* [bitbucket] Sử dụng cơ bản bitbucket để cập nhật phiên bản cho dự án Boardgame

## Kế hoạch tuần tới ##

* [wordpress] *Hoàn thành* dự án Boardgame 
* [sass] *Tìm hiểu* về Sass để thay cho Bootstrap



