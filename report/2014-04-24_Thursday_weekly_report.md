﻿# Thursday, 24 April 22:43:10 +0700 - Weekly report #

## Tổng kết ##

* [Boardgame] Hoàn thành 98% dự án boardgame
* [javascipt] Học được một số kiến thức cơ bản về javascipt
* [Freaking Math] Hoàn thành các task: Random, One-click, Local Storge and Social Shared

## Kế hoạch tuần tới ##

* [Freaking Math] Hoàn thành Freaking Math 
* [sass] Tìm hiểu về Sass để thay cho Bootstrap
* [javascipt] Học thêm về javascipt
* [clojure] Học các kiến thức căn bản về clojure